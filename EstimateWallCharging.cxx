/////////////////////////////////////////////
//
//  EstimateWallCharging.cxx
//
//  rlinehan@stanford.edu
//  January 14th, 2020
//
//  This code is meant to estimate
//  the amount of charging we'd see
//  on the PTFE walls if we run without
//  a filled water tank.
//
/////////////////////////////////////////////

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "TGraph.h"
#include "TH1F.h"

const int nBinsEnergy = 1000;
const double maxEnergy = 3000;

TGraph * ReadDigitizedPlotsFromSally();
TH1F * MakeEnergySpectrumHistFromGraph( TGraph * gRatePer3keV );
TH1F * ConvertEnergySpectrumToNElectronsCreated(TH1F * eSpect);
TGraph * ConvertNElectronsCreatedToPhotonsProduced( double nElectronsCreated, double enhancementFactor );
TGraph * PlotPhotonsProducedPerElectron();
TGraph * ConvertPhotonsProducedToChargeDensityOnWalls( TGraph * photonsProduced, double electronHoleProbability );

void EstimateWallCharging()
{

  //First things first: read in the rates of events from the spectrum that Sally made
  TGraph * gRatePer3keV = ReadDigitizedPlotsFromSally();
  TH1F * energySpectrum = MakeEnergySpectrumHistFromGraph(gRatePer3keV);

  //Sanity check: is the rate 15kHz?
  std::cout << "Integral of energy spectrum: " << energySpectrum->Integral() << std::endl;
  
  //Convert this energy spectrum to number of electrons created at each energy
  TH1F * nElectronsCreated = ConvertEnergySpectrumToNElectronsCreated(energySpectrum);

  //Integrate this to get the total number of electrons made per second
  std::cout << "Integral of nElectrons spectrum: " << nElectronsCreated->Integral() << std::endl;

  TCanvas * c2 = new TCanvas();
  c2->SetLogy();
  nElectronsCreated->SetLineColor(kRed);
  nElectronsCreated->Draw();

 
  
  //Produce graph of photons produced as a function of gate-anode deltaV (assuming cathode is > some threshold to get drift)
  double highFactor = 1.4;
  double lowFactor = 0.6;
  TGraph * photonsProduced_High = ConvertNElectronsCreatedToPhotonsProduced(nElectronsCreated->Integral(),highFactor);
  TGraph * photonsProduced_Center = ConvertNElectronsCreatedToPhotonsProduced(nElectronsCreated->Integral(),1.0);
  TGraph * photonsProduced_Low = ConvertNElectronsCreatedToPhotonsProduced(nElectronsCreated->Integral(),lowFactor);

  //Produce graph of photons absorbed
  TGraph * photonsAbsorbed_1p0 = ConvertPhotonsProducedToChargeDensityOnWalls(photonsProduced_Center,1.0);
  TGraph * photonsAbsorbed_0p1 = ConvertPhotonsProducedToChargeDensityOnWalls(photonsProduced_Center,0.1);
  TGraph * photonsAbsorbed_0p01 = ConvertPhotonsProducedToChargeDensityOnWalls(photonsProduced_Center,0.01);
  TGraph * photonsAbsorbed_0p001 = ConvertPhotonsProducedToChargeDensityOnWalls(photonsProduced_Center,0.001);

  //Photons produced per electron
  TGraph * photonsProducedPerElectron = PlotPhotonsProducedPerElectron();
  TCanvas * c3 = new TCanvas();
  photonsProducedPerElectron->SetLineColor(kBlue);
  photonsProducedPerElectron->Draw();
  
  
  
  
  
  TCanvas * c1 = new TCanvas();
  //nElectronsCreated->SetLineColor(kRed);
  //  nElectronsCreated->Draw();
  //  photonsProduced_High->SetLineColor(kBlue);
  //photonsProduced_High->Draw();
  //photonsProduced_Low->SetLineColor(kRed);
  //photonsProduced_Low->Draw("same");
  photonsAbsorbed_1p0->SetLineColor(kBlack);
  photonsAbsorbed_0p1->SetLineColor(kBlue);
  photonsAbsorbed_0p01->SetLineColor(kOrange);
  photonsAbsorbed_0p001->SetLineColor(kRed);
  photonsAbsorbed_1p0->SetLineWidth(2);
  photonsAbsorbed_0p1->SetLineWidth(2);
  photonsAbsorbed_0p01->SetLineWidth(2);
  photonsAbsorbed_1p0->SetLineStyle(9);
  photonsAbsorbed_0p1->SetLineStyle(9);
  photonsAbsorbed_0p01->SetLineStyle(9);
  
  photonsAbsorbed_0p001->SetLineWidth(4);

  
  photonsAbsorbed_1p0->Draw();
  photonsAbsorbed_0p1->Draw("same");
  photonsAbsorbed_0p01->Draw("same");
  photonsAbsorbed_0p001->Draw("same");
  c1->SetLogy();

  TLegend * l1 = new TLegend();
  l1->AddEntry(photonsAbsorbed_1p0,"Electron-hole probability = 1.0");
  l1->AddEntry(photonsAbsorbed_0p1,"Electron-hole probability = 0.1");
  l1->AddEntry(photonsAbsorbed_0p01,"Electron-hole probability = 0.01");
  l1->AddEntry(photonsAbsorbed_0p001,"Electron-hole probability = 0.001");
  l1->Draw("same");
  
}

TGraph * ConvertPhotonsProducedToChargeDensityOnWalls( TGraph * photonsProduced, double electronHoleProbability )
{
  double LCE = 0.06;
  double fractionOnPMTs = LCE*3;
  double fractionOnWalls = 1-fractionOnPMTs;

  //Get area of walls and teflon on top and bottom
  double teflonTopArea = 0.474; //m^2
  double teflonBottomArea = 0.474; //m^2;
  double teflonWallsArea = 7.32; //m^2
  double totalTeflonArea = teflonTopArea+teflonBottomArea+teflonWallsArea;
  
  double x,y;
  TGraph * g_Out = new TGraph();
  for( int iP = 0; iP < photonsProduced->GetN(); ++iP ){
    photonsProduced->GetPoint(iP,x,y);
    double nPhotonsOnWalls = fractionOnWalls*y;
    double chargeOnWalls = nPhotonsOnWalls*1.602e-19*electronHoleProbability;
    g_Out->SetPoint(iP,x,chargeOnWalls/totalTeflonArea);
  }
  g_Out->GetXaxis()->SetTitle("Gate-Anode DeltaV [V]");
  g_Out->GetYaxis()->SetTitle("Average Charge Density on TPC Teflon Added per Second [C]");

  return g_Out;
}

TGraph * PlotPhotonsProducedPerElectron()
{
  double a = 0.137; //In 1/V
  double b = 4.7e-18; //In cm^2

  double massDensity = 0.0162; //g/cm
  double avogadro = 6.02e23;
  double molarMassXe = 131; //g/cm^3
  double numDensity = massDensity/molarMassXe*avogadro;
    
  TGraph * g_Out = new TGraph();
  int nVoltages = 100;
  for( int iV = 0; iV < nVoltages; ++iV ){
    double voltage = iV*10.*1000./((double)nVoltages); //In V
    double field = voltage/1.3; //V/cm
    double nPhotonsPerCm = a*field - b*numDensity;
    if( nPhotonsPerCm < 0 ) nPhotonsPerCm = 0;
    g_Out->SetPoint(iV,voltage,nPhotonsPerCm*1.3);
  }
  g_Out->GetXaxis()->SetTitle("G-A DeltaV [V]");
  g_Out->GetYaxis()->SetTitle("Photons Per Electron");
  return g_Out;
}

    
TGraph * ConvertNElectronsCreatedToPhotonsProduced( double nElectronsCreated, double enhancementFactor ) //Enhancement factor is for uncertainty in event rate.
{
  double a = 0.137; //In 1/V
  double b = 4.7e-18; //In cm^2

  double massDensity = 0.0162; //g/cm
  double avogadro = 6.02e23;
  double molarMassXe = 131; //g/cm^3
  double numDensity = massDensity/molarMassXe*avogadro;
    
  TGraph * g_Out = new TGraph();
  int nVoltages = 100;
  for( int iV = 0; iV < nVoltages; ++iV ){
    double voltage = iV*10.*1000./((double)nVoltages); //In V
    double field = voltage/1.3; //V/cm
    double nPhotonsPerCm = a*field - b*numDensity;
    if( nPhotonsPerCm < 0 ) nPhotonsPerCm = 0;
    g_Out->SetPoint(iV,voltage,nPhotonsPerCm*1.3*nElectronsCreated*enhancementFactor);
  }
  g_Out->GetXaxis()->SetTitle("G-A DeltaV [V]");
  g_Out->GetYaxis()->SetTitle("Photons Per Second");
  return g_Out;
}

TH1F * ConvertEnergySpectrumToNElectronsCreated(TH1F * eSpect)
{
  double W_eV = 22.0;
  TH1F * h_Out = new TH1F("h_PhotonsCreated","Electrons Created; Event Energy [keV]; Electrons Created per 3keV, per second;",nBinsEnergy,0,maxEnergy);
  for( int iB = 0; iB < eSpect->GetNbinsX(); ++iB ){
    double eVal = eSpect->GetBinLowEdge(iB) + eSpect->GetBinWidth(iB)/2.0;
    double nElectrons = eVal*1000./W_eV;
    h_Out->SetBinContent(iB,nElectrons*eSpect->GetBinContent(iB));
  }
  return h_Out;
}

TH1F * MakeEnergySpectrumHistFromGraph( TGraph * gRatePer3keV )
{
  TH1F * hOut = new TH1F("h_EnergySpectrum","Energy Spectrum of Gas Events, No Water Tank; Energy [keV]; Rate/3Hz",nBinsEnergy,0,maxEnergy);
  for( int iB = 0; iB < hOut->GetNbinsX(); ++iB ){
    double energyVal = hOut->GetBinLowEdge(iB)+hOut->GetBinWidth(iB)/2.0;
    double ratePer3keV = gRatePer3keV->Eval(energyVal);
    if( ratePer3keV < 0 ) ratePer3keV = 0;
    hOut->SetBinContent(iB,ratePer3keV);
  }
  return hOut;
}


TGraph * ReadDigitizedPlotsFromSally()
{
  ifstream infile;
  infile.open("/Users/ryanlinehan/LZ_Local_Work_2020/Commissioning/WallCharging/TPCRatesInGasNoWaterTank.csv");
  double energy, ratePer3keV;
  TGraph * g1 = new TGraph();
  int counter = 0;
  while(1){
    infile >> energy >> ratePer3keV;
    if(!infile.good()) break;
    std::cout << "Adding point to graph." << std::endl;
    g1->SetPoint(counter,energy,ratePer3keV);
    counter++;
  }
  infile.close();
  return g1;
}
